$("#newAccount").click(function () {

    $("#newAccount").css("display", "none")

        // si le cookie est vide on créer un nouvel utilisateur
        if (getCookie('authKey') === '') {
            $.ajax('https://api.messenger.codecolliders.dev/createUser').done(function (user) {
                currentUser = user;
                setCookie('authKey', user.authKey, 10);
                getMessages();
            });
        } else {
            // sinon on récupère l'utilisateur existant
            $.ajax({
                url: 'https://api.messenger.codecolliders.dev/getUser',
                method: 'POST',
                data: {
                    authKey: getCookie('authKey'),
                }
            }).done(function (user) {
                user.authKey = getCookie('authKey');
                currentUser = user;
                getMessages();
            });
        }

// définir un cookie
// https://www.w3schools.com/js/js_cookies.asp
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

// récupération d'un cookie
// https://www.w3schools.com/js/js_cookies.asp
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

//récupération users
    $.ajax("https://api.messenger.codecolliders.dev/getUsers").done(function (data) {
        for (let i = 0; i < data.length; i++) {
            $('#users').append('<div class="usersNames" onclick="test(' + data[i].id + ')">' + data[i].username + '</div>');
        }
    })

    var sendMessage = $("#sendMessage")
    var to = 0

//jveu l'id dans le onclick
    function test(info) {
        return to = info
    }

// envoi d'un message vers l'api
    sendMessage.keypress(function (event) {
        if (event.key === "Enter") {
            $.ajax({
                url: 'https://api.messenger.codecolliders.dev/sendMessage',
                method: 'POST',
                data: {
                    authKey: currentUser.authKey,
                    text: sendMessage.val(),
                    to: to,
                }
            }).done(function () {
                clearTimeout(timeout);
                getMessages();
            });
            sendMessage.val('');
        }
    });

    var lastId = 0;
    var timeout;
    var delay = 100;

// chargement des nouveaux messages
    function getMessages() {
        $.ajax({
            url: 'https://api.messenger.codecolliders.dev/getMessages',
            method: 'POST',
            data: {
                authKey: currentUser.authKey,
                lastId: lastId,
            }
        }).done(function (messages) {
            // affichage des nouveaux messages dans la page
            for (i = 0; i < messages.length; i++) {
                $('#messages').append('<div class="displayMessages">' + messages[i].from.username + ': ' + messages[i].text + '</div>');
                lastId = messages[i].id;
            }

            if (messages.length === 0) {
                delay += 100;
            } else {
                delay = 100;
            }

            timeout = setTimeout(getMessages, delay);
        });
    }


//changer son nom d'utilisateur
    function changeUsername(newUsername) {
        $.ajax({
            url: 'https://api.messenger.codecolliders.dev/changeUsername',
            method: 'POST',
            data: {
                authKey: currentUser.authKey,
                username: newUsername,
            },
        })
    }

//NEW PSEUDO
    $("#confirmer").click(function () {
        var newUsername = $("#inputNewNickname").val()
        changeUsername(newUsername)
        $('#buttonNewUsername > input[type="text"]').val('')

        console.log(newUsername);
    })
})


// ON RESIZE + ONLOAD
// function loadAndResize() {
//     if (window.innerWidth < 720) {
//         $("#hideChange").css("display", "none")
//     } else {
//         $("#hideChange").css("display", "block")
//     }
// }
// window.onload = window.onresize = loadAndResize;